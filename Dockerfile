ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_python:3.8.2-headless as opt_python

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_tools:19.04 as build

COPY --from=opt_python /opt /opt

ENV PATH /opt/bin/:${PATH}
ENV LD_LIBRARY_PATH /opt/lib/:${LD_LIBRARY_PATH}

RUN pip install prody
RUN pip install scipy

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_python:3.8.2-headless

COPY --from=build /opt /opt

COPY bin/bioutils.extract_chains_and_sequences /opt/bin/
COPY bin/bioutils.merge_pairwise_alignments /opt/bin/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module
